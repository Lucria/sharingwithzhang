using System;

namespace dotnet_server_demo.Models
{
    public class Student
    {
        public Student(Guid id, string name, int age)
        {
            this.Id = id;
            this.Name = name;
            this.Age = age;
        }
        
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public int Age { get; private set; }

    }
}