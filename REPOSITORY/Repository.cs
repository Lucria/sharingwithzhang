using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_server_demo.Models;
using Microsoft.EntityFrameworkCore;


namespace dotnet_server_demo.REPOSITORY
{
    public class Repository:IRepository
    {
        private readonly DatabaseContext _databaseContext;
        public Repository(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        
        public IEnumerable<Student> GetAllStudents()
        {
            return mapDatatoRepo(_databaseContext.students);
        }

        public Student GetStudent(Guid id)
        {
            return dataToDomain(
                _databaseContext.students.Find(
                    id.ToString()
                )
            ); // Should return student la
        }

        public Student PostStudent(Student student)
        {
            _databaseContext.students.Add(domainToData(student));
            _databaseContext.SaveChanges();
            return student;
        }


        public Student PutStudent(Guid id, Student student)
        {
            Data updatedStudent = _databaseContext.students.Find(id.ToString());
            updatedStudent = domainToData(student);
            _databaseContext.SaveChanges();
            return student;
        }

        public void DeleteStudent(Guid id)
        {
            Data toBeDeleted = _databaseContext.students.Find(id.ToString());
            _databaseContext.Remove(toBeDeleted);
            _databaseContext.SaveChanges();
        }
        
        public IEnumerable<Student> mapDatatoRepo(IEnumerable<Data> everything)
        {
            return everything.Select(x => dataToDomain(x));
            
        }

        public Student dataToDomain(Data data)
        {
            Guid id = Guid.Parse(data.Id);
            string name = data.Name;
            int age = int.Parse(data.Age);
            return new Student(id, name, age); 
        }

        public Data domainToData(Student student)
        {
            Data data = new Data(student.Id.ToString(), student.Name,student.Age.ToString());
            return data;
        }
    }
}