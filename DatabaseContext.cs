using System.Collections;
using System.Collections.Generic;
using dotnet_server_demo.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnet_server_demo
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        
        public DbSet<Data> students { get; set; }
    }
}