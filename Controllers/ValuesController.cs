﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnet_server_demo.Models;
using dotnet_server_demo.REPOSITORY;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_server_demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IRepository _repository;

        public StudentController(IRepository repository)
        {
            _repository = repository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Student>> Get()
        {
            return Ok(_repository.GetAllStudents());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Student> Get(Guid id)
        {
            return Ok(_repository.GetStudent(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Student> Post([FromBody] StudentRequest studentRequest)
        {
            Student createdStudent = new Student(Guid.NewGuid(), studentRequest.Name, studentRequest.Age);
            _repository.PostStudent(createdStudent);
            return Created(Request.Path.Value + "/" + createdStudent.Id, createdStudent);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<Student> Put(Guid id, [FromBody] Student studentRequest)
        {
            return Ok(_repository.PutStudent(id, studentRequest));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            _repository.DeleteStudent(id);
            return NoContent();
        }
    }
}
